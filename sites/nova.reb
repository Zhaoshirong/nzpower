Rebol []

rates: [
    make object! [
        name: "Nova - Regular"
        Date: 24/3/2019
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.25714
            solar-rebate: $0.0805 ; guess
            ; controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $2.10
        ]
    ]
    make object! [
        name: "Nova - Low User"
        Date: 24/03/2019
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.25714
            solar-rebate: $0.0805 ; Guess
            ; controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $0.41
        ]
    ]
]
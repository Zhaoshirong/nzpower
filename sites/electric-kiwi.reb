Rebol []

rates: [
  make object! [
    name: "Electric Kiwi Low User - Loyal Kiwi"
    location: "Wellington"
    notes: "one year commitment"
    kw-charge: make object! [
      kwH: $0.2744
      solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $0.3400
    ]
 ]
 
 make object! [
    name: "Electric Kiwi Low User - Kiwi"
    location: "Wellington"
    notes: "everyday great rates"
    kw-charge: make object! [
      kwH: $0.2890
      solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $0.3400
   ]
 ]
  
  make object! [
    name: "Electric Kiwi Low User - Stay Ahead 200"
    location: "Wellington"
    notes: "10% top-up bonus"
    kw-charge: make object! [
      kwH: $0.2992
      solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $0.3700    
    ]
 ]
 
 make object! [
    name: "Electric Kiwi Standard User - Loyal Kiwi"
    location: "Wellington"
    notes: "one year commitment"
    kw-charge: make object! [
      kwH: $0.2140
      solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $1.6400
    ]
 ]
 
 make object! [
   name: "Electric Kiwi Standard User - Kiwi"
   location: "Wellington"
    notes: "one year commitment"
    kw-charge: make object! [
        kwH: $0.2197
        solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $1.86
   ]
 ]
  
make object! [
   name: "Electric Kiwi Standard User - Stay Ahead 200"
   location: "Wellington"
    notes: "10% top-up bonus"
    kw-charge: make object! [
      kwH: $0.2354
      solar-rebate: $0
    ]
    daily-charge: make object! [
      fixed: $1.7700
   ]
 ]
]
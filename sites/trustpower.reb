Rebol []

rates: [
    make object! [
        name: "TrustPower - Regular"
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.24564
            solar-rebate: $0.0805
            controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $2.4495 
        ]
    ]
    make object! [
        name: "TrustPower - Low User"
        Date: 24/03/2019
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.340515
            solar-rebate: $0.0805 ; Guess
            ; controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $0.41
        ]
    ]
]
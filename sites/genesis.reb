Rebol []

rates: [
    make object! [
        name: "Genesis - Regular"
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.247365
            solar-rebate: $0.0805 ; guess
            ; controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $2.58
        ]
    ]
    make object! [
        name: "Genesis - Low User"
        Date: 24/03/2019
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.22908
            solar-rebate: $0.0805 ; Guess
            ; controlled: $0.1788249
        ]
        daily-charge: make object! [
            daily-fixed-charges: $1.99
        ]
    ]
]
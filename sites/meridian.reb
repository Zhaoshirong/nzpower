Rebol []

rates: [
    make object! [
        name: "Meridian - 3 year fixed"
        location: "Wellington"
        notes: {3 years, $300 credit}
        kw-charge: make object! [
            kwH: $0.1626
            solar-rebate: $0.08
            electricity-authority-levy: $0.00145475
        ]
        daily-charge: make object! [
            daily-fixed-charges: $1.6997
        ]
    ]
    make object! [
        name: "Meridian - 3 year fixed low user"
        location: "Wellington"
        notes: {3 years, $300 credit}
        kw-charge: make object! [
            kwH: $0.2539
            solar-rebate: $0.08
            electricity-authority-levy: $0.00145475
        ]
        daily-charge: make object! [
            daily-fixed-charges: $0.30
        ]
    ]
]

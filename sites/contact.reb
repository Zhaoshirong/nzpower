Rebol []

rates: [
    make object! [
        name: "Contact - Regular"
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.186
            solar-rebate: $0.0805 ; guess
            controlled: $0.1334
            energy-authority: $0.001495
        ]
        daily-charge: make object! [
            daily-fixed-charges: $1.84
        ]
    ]
    make object! [
        name: "Contact - Low User"
        Date: 24/03/2019
        location: "Wellington"
        notes: {no contract, dual fuel}
        kw-charge: make object! [
            kwH: $0.347875
            solar-rebate: $0.0805 ; Guess
            controlled: $0.1788249
            energy-authority: $0.001495
        ]
        daily-charge: make object! [
            daily-fixed-charges: $0.38
        ]
    ]
]

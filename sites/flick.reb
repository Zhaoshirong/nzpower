Rebol []

rates: [
    make object! [
        name: "Flick - Fixie"
        location: "Napier"
        Date: 23-April-2019
        notes: {6 month contract - ?? no solar rebate}
        kw-charge: make object! [
            kwH: $0.08993
            solar-rebate: $0.0
            network-line-charges-uncontrolled: $0.11385
            flick-meter: $0.01817
            flick-fixie-meter: $0.001725
            electricity-authority-levy: $0.0012995
        ]
        daily-charge: make object! [
            network-daily-charges: $1.725
            metering-charges: $0.29325
            flick-access-charge: $0.483
        ]
    ]
]

Rebol []

rates: [
    make object! [
        name: "Mercury - Everyday user"
        location: "Wellington"
        notes: {no contract}
        kw-charge: make object! [
            kwH: $0.2428799999
            solar-rebate: $0.08
            electricity-authority-levy: $0.00145475
        ]
        daily-charge: make object! [
            daily-fixed-charges: $2.5834749
        ]
    ]
    make object! [
        name: "Mercury - low user"
        location: "Wellington"
        notes: {no contract}
        kw-charge: make object! [
            kwH: $0.3431599999
            solar-rebate: $0.08
            electricity-authority-levy: $0.00145475
        ]
        daily-charge: make object! [
            daily-fixed-charges: $0.3832949
        ]
    ]
]
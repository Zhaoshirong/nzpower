Rebol []

rates: [
    make object! [
        name: "EnergyClub - standard user uncontrolled"
        location: "Wellington"
        notes: {no contract}
        kw-charge: make object! [
            kwH: $0.090045
            solar-rebate: $0.092
            network-line-charges-uncontrolled: $0.083375
            electricity-authority-levy: $0.001265
        ]
        daily-charge: make object! [
            metering-charges: $0.302466
            network-line-charges-fixed: $1.265
            industry-levies: $0.008489
            club-fees: $0.7142857142857143
        ]
    ]
]
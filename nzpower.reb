rebol [
    date: 16-Mar-2019
    author: "Graham Chiu"
    notes: {A function 'nzpower to calculate rates from NZ power suppliers}
    version: 0.1.101
]

sites: %sites/sites.reb  ; will load relative to where this script is run from
directories: https://gitlab.com/api/v4/projects/11340891/repository/tree?path=sites&ref=Development

root: what-dir ; https://gitlab.com/Zhaoshirong/nzpower/raw/Development/ ; sites/electric-kiwi.reb 

if not find form system/version "0.16." [
    replpad-write: function [t /html /note][print t]
    clear-screen: does []
]
;; == bug test
;; GST: 1.15

print "loading JSON..."
import @json

; red text
fred: func [txt /newln][
    replpad-write/html unspaced [<font color=red> txt </font>]
    if newln [print newline]
]

; bold text
fbold: func [txt /newln][
    replpad-write/html unspaced [<i> txt </i>]
    if newln [print newline]
]

; note text
fnote: func [txt][
    replpad-write/html unspaced [<div class="w3-panel w3-yellow"> txt </div>]
]

nzpower: adapt :console [
    clear-screen
    GSTexc: func [num][
        round/to num / 1.15 $0.01
    ]
    GSTinc: func [num][
        round/to num * 1.15 $0.01
    ]
    cheapest: make object! [
        site: _ cost: _ 
    ]

    fnote spaced [
        {This utility calculates what your power bill might be with different providers based on information you provide.  It does need a power bill showing the number of days, and the units charged.}
        {Only a couple of providers are included initially.}
    ]

    if file! = type-of sites [
        fbold/newln "loading site data..."
        ; load the directory data as JSON and convert to a Rebol map!
        sites: load-json to text! read directories
        ; sites: reduce do to text! read directories
        t: copy []
        for-each site sites [
            if %.reb = suffix-of site/name [
                ; grab the filename for the config file
                temp: to tag! site/path  ; TAG!s are script-relative paths
                print ["Trying to read:" site/name "from" mold temp]
                sites: reduce do temp
                append t sites
            ]
        ]
        sites: copy t
    ]

    cycle [
        prin "Enable JavaScript tracing? (y/N) "
        js-trace ("y" = input)
        fnote "If you get a divide by 0 error, then you do not have <strong>webassembly threads enabled</strong>. Set this in chrome://flags"
        cheapest/cost: $1'000'000'00 cheapest/site: _

        print newline
        
        prin "Do you have an enticing custom offer? (y/N)? " offer: input
        if offer = "y" [
            prin "Name of supplier? " supplier: input
            if empty? supplier [supplier: copy "Custom"]
            fbold/newln "Enter GST inclusive cent values for the following"
            cycle [
                prin "Price in cents per kWh: " cents: input
                if any-number? cents: load cents [
                    pkwH: to money! cents / 100
                    break
                ]
            ]
            cycle [
                prin "Daily charge in cents: " cents: input
                if any-number? cents: load cents [
                    daily: to money! cents / 100
                    break
                ]
            ]
            cycle [
                prin "Solar Rebate in cents: " cents: input
                if empty? cents [
                    cents: 0
                ]
                if any-number? cents: load cents [
                    solar: to money! cents / 100
                    break
                ]
            ]
            custom: make object! [
                name: :supplier
                location: "Not stated"
                notes: {Custom offer}
                kw-charge: make object! [
                    kwH: :pkWh
                    solar-rebate: :solar
                    electricity-authority-levy: $0.00145475
                ]
                daily-charge: make object! [
                    daily-fixed-charges: :daily
                ]
            ]
            append sites custom
        ]
        
        
        days: ask "Enter whole days covered by this bill (Q): "

        if days = "Q" [quit]

        if not attempt [days: to-integer days] [
            print "Needs integer value for days of power bill"
            continue
        ]

        if zero? days [stop]

        kWh: ask "Enter kWh used over this bill: "

        if not attempt [kwH: to decimal! kWh][
            print "A valid number is required for power consumption"
            continue
        ]

        prin "Enter any controlled or night rate kWh (in decimal dollars) in this bill (0): "
        controlled: 0 
        attempt [controlled: to decimal! input]
        print "Enter kWh exported to the Grid over this period."
        prin "Enter 0 or return if you don't have solar panels! "
        ep: 0
        attempt [ep: to decimal! input]

        until [prin "Cost of your bill including GST in $" money? attempt [bill: to money! input]]
        

        ; got valid days and kWh so now calculate various scenarios
        
        output-table: copy {<table style="width:60%" border="1"><tr><th align="left">Company</th><th align="left">Location</th><th align="left">Calculated Charge</th></tr>}

        for-each site sites [ ; name location kw-charge daily-charge
            site-words: words-of site
            catch [
                for-each w [name location notes kw-charge daily-charge][
                    if not find site-words w [
                        ; exit to next site as this one is missing values
                        fred/newln spaced ["This site is missing values or is corrupted:" newline mold site]
                        throw "site is missing members"
                    ]
                ]
                ; site is okay
                total: $0
                fred/newln site/name
                print site/location
                fbold/newln site/notes
                ; fbold/newln "Charges per kWh"
                html: copy [<table style="width:40%" border="1"> <tr> <th align="left"> "Charge Type" </th> <th align="left"> "Rate" </th> <th align="left"> "Ex GST" </th> <th align="left"> "Inc GST" </ht> </tr>]
                rows: collect [
                    for-each charge words-of site/kw-charge [
                            if money! = type-of site/kw-charge/:charge [
                                case [
                                    charge = 'solar-rebate [
                                        c: negate ep * site/kw-charge/:charge $0.000001 
                                        ; prin [ charge round/to c: negate ep * site/kw-charge/:charge $0.000001 "(Rate:"  site/kw-charge/:charge ")"]
                                    ]
                                    charge = 'controlled [
                                        c: controlled * site/kw-charge/:charge $0.000001
                                        ; prin [ charge round/to c: controlled * site/kw-charge/:charge $0.000001 "(Rate:"  site/kw-charge/:charge ")"]
                                    ]
                                    all [charge = 'kwH not find words-of site/kw-charge 'controlled][
                                        c: (kwH + controlled) * site/kw-charge/:charge $0.000001 
                                        ; prin [ charge round/to c: (kwH + controlled) * site/kw-charge/:charge $0.000001 "(Rate:"  site/kw-charge/:charge ")"]
                                    ]
                                    charge = 'kwH [
                                        c: kwH * site/kw-charge/:charge $0.000001 
                                        ; prin [ charge round/to c: kwH * site/kw-charge/:charge $0.000001 "(Rate:"  site/kw-charge/:charge ")"]
                                    ]
                                    true [
                                        ; keep {<td> "other" </td>}
                                        ; prin [ charge round/to c: (kwH + controlled) * site/kw-charge/:charge $0.000001 "(Rate:"  site/kw-charge/:charge ")"]
                                        c: (kwH + controlled) * site/kw-charge/:charge $0.000001 
                                    ]
                                ]

probe site
probe site/kw-charge
probe charge
probe site/kw-charge/:charge


                                keep unspaced [<tr> <td> charge </td> <td> site/kw-charge/:charge </td> <td> round/to c / 1.15 $0.01  </td> <td> round/to c $0.01 </td> </tr>]

print "row calculated"
probe c

                            ; print ["[GST exc" round/to c / 1.15 $0.01"]"]
                            total: total + c
                        ]
                    ]
                    ; fbold/newln "Charges per Day"
                    for-each charge words-of site/daily-charge [
                        if money! = type-of site/daily-charge/:charge [
                            c: days * site/daily-charge/:charge 
                            keep unspaced [<tr> <td> charge </td> <td> site/daily-charge/:charge </td> <td> round/to c / 1.15 $0.01  </td> <td> round/to c $0.01 </td> </tr>]
                            ; print ["[GST exc" round/to c / 1.15 $0.01"]"]
                            total: total + c
                        ]
                    ]
                    keep </table>
                ]
                append html rows
                replpad-write/html form html
                fnote spaced ["Total:" round/to total $0.01]
                print newline
                if cheapest/cost > total [
                    cheapest/cost: total
                    cheapest/site: copy site/name
                ]
                
                append output-table unspaced [
                    <tr>
                    <td> site/name </td>
                    <td> site/location </td>
                    <td> round/to total $0.01 </td>
                    </tr>
                ]
            ]
        ]
        
        append output-table </table>
        
        replpad-write/html output-table

        if bill <> $0 [
            savings: round/to (cheapest/cost - bill) $0.01
        ]
        replpad-write/html "<hr>"

        replpad-write/html spaced [<div class="w3-panel w3-yellow"> <i> "Duration of bill in days:" </i> <b> days </b> <i> "kWh:" </i> <b> kWh </b> <i> "Controlled:" </i>  <b> controlled </b> <i> "Generation:" </i> <b> ep </b> <i> "Cost:" </i> <b> bill </b> </div>]

        fnote spaced [
            "Cheapest was <b>" cheapest/site "</b> at <font color=red>" round/to cheapest/cost $0.01 </font> <br/>
            spaced [
                if bill <> $0 [
                    spaced [
                        if positive? savings ["Great, your savings were:"] else
                        ["Sadly, you were overcharged by:"] 
                        abs savings
                    ]
                ]
            ] 
        ]

        fred/newln "Start New Calculation"
    ]
]

; remove this line if you won't want the function to run on loading
nzpower
